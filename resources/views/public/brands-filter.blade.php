@push('additional-filters')
    <div class="filter brands-filter">
        <div class="name">Marcas</div>
        <div class="brands-list">
            <div>
                <a href="#" wire:click="$set('additionalFilters.brand', -1)" @if(isset($additionalFilters['brand']) && $additionalFilters['brand'] == -1) class="selected" @endif>Todas</a>
            </div>
            @foreach($availableBrands as $availableBrand)
                <div>
                    <a href="#" wire:click="$set('additionalFilters.brand', {{ $availableBrand->id }})"
                    @if(isset($additionalFilters['brand']) && $additionalFilters['brand'] == $availableBrand->id) class="selected" @endif>{{ $availableBrand->name }}</a>
                </div>
            @endforeach
        </div>
    </div>
@endpush
