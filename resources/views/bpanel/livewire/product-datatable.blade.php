<td>
    {{ $row->name }}
</td>
<td>
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-brand-'.$row->id))
</td>
<td>
    @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-product-brands', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'la marca?'], key('bpanel4-product-brands-'.$row->id))
</td>