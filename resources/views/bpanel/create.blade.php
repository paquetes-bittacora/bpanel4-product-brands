@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-product-brands::bpanel.create'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-product-brands::bpanel.create') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @include('bpanel4-product-brands::bpanel._form')
        </div>
    </div>
@endsection
