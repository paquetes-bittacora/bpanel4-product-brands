@pushonce('before-active-additional-fields')
    @livewire('form::select', [
        'name' => 'brand',
        'allValues' => $brands,
        'labelText' => __('bpanel4-product-brands::brand.brand'),
        'selectedValues' => [$product?->brand()->first()?->getId()],
        'emptyValue' => true,
        'emptyValueText' => 'Sin marca',
        'multiple' => false,
        'sortAlphabetically' => true
    ])
@endpushonce
