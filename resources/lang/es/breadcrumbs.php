<?php

declare(strict_types=1);

return [
    'bpanel4-product-brands' => 'Marcas',
    'index' => 'Listado',
    'create' => 'Nueva',
    'edit' => 'Editar',
];
