<?php

declare(strict_types=1);

return [
    'create' => 'Nueva',
    'edit' => 'Editar',
];
