<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Tests\Acceptance\Http\Controllers;

use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class BrandedProductsAdminControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testMuestraElCampoDeMarcaAlCrearUnProducto(): void
    {
        $this->actingAs((new UserFactory())->withPermissions('bpanel4-products.create')->createOne());
        $response = $this->get('/bpanel/productos/nuevo');
        $response->assertSee("Marca");
    }

    public function testMuestraElCampoDeMarcaAlEditarUnProducto(): void
    {
        $this->actingAs((new UserFactory())->withPermissions('bpanel4-products.edit')->createOne());
        $product = (new ProductFactory())->createOne();
        $response = $this->get('/bpanel/productos/' . $product->getId() .'/editar');
        $response->assertSee("Marca");
    }
}
