<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Tests\Unit\Listeners;

use Bittacora\Bpanel4\ProductBrands\Listeners\SaveProductBrand;
use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\Products\Events\ProductCreated;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Tests\TestCase;

final class SaveProductBrandListenerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testActualizaLaMarcaDelProducto(): void
    {
        $brand = $this->createBrand();
        $product = (new ProductFactory())->createOne();
        $listener = new SaveProductBrand();
        $listener->handle(new ProductCreated($product, new Request(request: ['brand' => $brand->getId()])));
        $this->assertCount(1, $product->refresh()->brand);
    }

    private function createBrand(): ProductBrand
    {
        $brand = new ProductBrand();
        $brand->setName($this->faker->name);
        $brand->setActive(true);
        $brand->save();

        return $brand;
    }
}
