<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Tests\Unit\Actions;

use Bittacora\Bpanel4\ProductBrands\Actions\CreateProductBrand;
use Bittacora\Bpanel4\ProductBrands\Dtos\ProductBrandDto;
use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class CreateProductBrandTest extends TestCase
{
    use RefreshDatabase;

    private CreateProductBrand $createBrand;

    protected function setUp(): void
    {
        parent::setUp();
        $this->createBrand = $this->app->make(CreateProductBrand::class);
    }

    /**
     * @throws Exception
     */
    public function testCreaUnaMarca(): void
    {
        $name = 'Marca de prueba ' . random_int(0, 1000);
        $dto = new ProductBrandDto($name, true);

        $this->createBrand->execute($dto);

        $this->assertDatabaseHas(ProductBrand::class, ['name' => json_encode(['es' => $name], JSON_THROW_ON_ERROR)]);
    }
}
