<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Tests\Unit\Actions;

use Bittacora\Bpanel4\ProductBrands\Actions\SaveProductBrand;
use Bittacora\Bpanel4\ProductBrands\Database\Factories\ProductBrandFactory;
use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class SaveProductBrandTest extends TestCase
{
    use RefreshDatabase;

    private SaveProductBrand $saveProductBrand;

    protected function setUp(): void
    {
        parent::setUp();
        $this->saveProductBrand = $this->app->make(SaveProductBrand::class);
    }

    public function testGuardaLaMarcaDeUnProducto(): void
    {
        $product = (new ProductFactory())->createOne();
        $brand = (new ProductBrandFactory())->createOne();

        $this->saveProductBrand->execute($product, $brand);

        // Lo hago así para que PHPStan no se queje, porque por tipado siempre será true pero quiero hacer este
        // test aún así para comprobar de forma general el funcionamiento de esta parte del plugin.
        $this->assertEquals(ProductBrand::class, $product->brand()->firstOrFail()::class);
    }
}
