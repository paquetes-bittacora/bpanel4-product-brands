<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Tests\Unit\Actions;

use Bittacora\Bpanel4\ProductBrands\Actions\UpdateProductBrand;
use Bittacora\Bpanel4\ProductBrands\Database\Factories\ProductBrandFactory;
use Bittacora\Bpanel4\ProductBrands\Dtos\ProductBrandDto;
use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class UpdateProductBrandTest extends TestCase
{
    use RefreshDatabase;

    private UpdateProductBrand $updateBrand;

    protected function setUp(): void
    {
        parent::setUp();
        $this->updateBrand = $this->app->make(UpdateProductBrand::class);
    }

    /**
     * @throws Exception
     */
    public function testCreaUnaMarca(): void
    {
        $brand = (new ProductBrandFactory())->createOne();
        $name = 'Marca de prueba ' . random_int(0, 1000);
        $dto = new ProductBrandDto($name, true, $brand->getId());

        $this->updateBrand->execute($dto);

        $this->assertDatabaseHas(ProductBrand::class, [
            'id' => $brand->getId(),
            'name' => json_encode(['es' => $name], JSON_THROW_ON_ERROR),
        ]);
    }
}
