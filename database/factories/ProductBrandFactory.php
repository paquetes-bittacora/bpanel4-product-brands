<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Database\Factories;

use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ProductBrand>
 * @method ProductBrand createOne($attributes = [])
 */
final class ProductBrandFactory extends Factory
{
    /** @var class-string<ProductBrand> */
    protected $model = ProductBrand::class;

    /**
     * @return array{name: string, active: true}
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'active' => true,
        ];
    }
}
