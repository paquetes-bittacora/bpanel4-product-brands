<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Bpanel4\ProductBrands\Commands\Helpers\RegisterProductModelTraitsHelper;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    public $signature = 'bpanel4-product-brands:install';

    private const PERMISSIONS = [
        'index',
        'create',
        'show',
        'edit',
        'destroy',
        'store',
        'update',
    ];

    public function handle(RegisterProductModelTraitsHelper $registerProductModelTraitsHelper, AdminMenu $adminMenu): void
    {
        $registerProductModelTraitsHelper->extendProductClass();
        $this->createMenuEntries($adminMenu);
        $this->createTabs();
        $this->giveAdminPermissions();
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-product-brands.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createTabs(): void
    {
        Tabs::createItem(
            'bpanel4-product-brands.index',
            'bpanel4-product-brands.index',
            'bpanel4-product-brands.index',
            'Marcas',
            'fa fa-list'
        );

        Tabs::createItem(
            'bpanel4-product-brands.index',
            'bpanel4-product-brands.create',
            'bpanel4-product-brands.create',
            'Nueva',
            'fa fa-plus'
        );
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule('ecommerce', 'bpanel4-product-brands', 'Marcas', 'fas fa-award');
        $adminMenu->createAction('bpanel4-product-brands', 'Listado', 'index', 'fas fa-list');
        $adminMenu->createAction('bpanel4-product-brands', 'Nuevo', 'create', 'fas fa-plus');
    }
}
