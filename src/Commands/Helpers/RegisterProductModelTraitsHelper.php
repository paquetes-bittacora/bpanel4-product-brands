<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Commands\Helpers;

use App\Traits\Bpanel4ProductTraits;
use Bittacora\Bpanel4\ProductBrands\Traits\HasBrand;
use ReflectionClass;
use RuntimeException;

final class RegisterProductModelTraitsHelper
{
    public function extendProductClass(): void
    {
        $path = $this->getPath();
        $originalContent = $this->getFileGetContents($path);

        if (\Illuminate\Support\Str::contains($originalContent, HasBrand::class)) {
            return;
        }

        $modifiedContent = str_replace("}", "    use \\" . HasBrand::class . ";\n}", $originalContent);

        file_put_contents($path, $modifiedContent);
    }

    private function getPath(): string
    {
        $reflector = new ReflectionClass(Bpanel4ProductTraits::class);
        $path = $reflector->getFileName();

        if (false === $path) {
            throw new RuntimeException();
        }
        return $path;
    }

    private function getFileGetContents(string $path): string
    {
        $originalContent = file_get_contents($path);

        if (false === $originalContent) {
            throw new RuntimeException();
        }
        return $originalContent;
    }
}
