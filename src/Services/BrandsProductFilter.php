<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Services;

use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Bittacora\Bpanel4\Products\Contracts\ProductsFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\View\View;

final class BrandsProductFilter implements ProductsFilter
{
    public function addToView(View $view): void
    {
        $view->nest(
            'additional-filters',
            'bpanel4-product-brands::public.brands-filter',
            $view->getData() +
            ['availableBrands' => ProductBrand::where('active', true)->orderBy('name')->get()->all()]
        );
    }

    /**
     * @param array<string, string|string[]|int|int[]> $filters
     */
    public function filterQuery(Builder $query, array $filters): void
    {
        $brand = $this->getBrandId($filters);

        if (null !== $brand) {
            $query->whereHas('brand', static function (Builder $query) use ($brand): void {
                $query->where('product_brands.id', $brand);
            });
        }
    }

    /**
     * @param array<string, string|string[]|int|int[]> $filters
     */
    private function getBrandId(array $filters): ?int
    {
        if (!$this->filtersContainBrandFilter($filters)) {
            return null;
        }

        $brand = $filters['additionalFilters']['brand'];
        $brand = "-1" === $brand ? null : (int)$brand;

        return (null === $brand || -1 === $brand) ? null : $brand;
    }

    /**
     * @param array<string, string|string[]|int|int[]> $filters
     * @phpstan-assert-if-true array{additionalFilters: array{brand: string|int}} $filters
     */
    public function filtersContainBrandFilter(array $filters): bool
    {
        return [] !== $filters && isset($filters['additionalFilters']['brand']);
    }
}
