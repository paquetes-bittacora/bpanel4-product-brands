<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Actions;

use Bittacora\Bpanel4\ProductBrands\Dtos\ProductBrandDto;
use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;

final class CreateProductBrand
{
    public function execute(ProductBrandDto $dto): ProductBrand
    {
        $brand = new ProductBrand();
        $brand->setName($dto->name);
        $brand->setActive($dto->active);
        $brand->save();

        return $brand;
    }
}
