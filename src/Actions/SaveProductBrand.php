<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Actions;

use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Bittacora\Bpanel4\Products\Models\Product;

final class SaveProductBrand
{
    public function execute(Product $product, ProductBrand $brand): void
    {
        $product->brand()->save($brand);
    }
}
