<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Actions;

use Bittacora\Bpanel4\ProductBrands\Dtos\ProductBrandDto;
use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use RuntimeException;

final class UpdateProductBrand
{
    public function execute(ProductBrandDto $dto): ProductBrand
    {
        if (null === $dto->id) {
            throw new RuntimeException('El id de la marca no puede estar vacío al intentar actualizarla');
        }

        $brand = ProductBrand::whereId($dto->id)->firstOrFail();
        $brand->setName($dto->name);
        $brand->setActive($dto->active);
        $brand->save();

        return $brand;
    }
}
