<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Http\Livewire;

use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class BrandsDatatable extends DataTableComponent
{
    /**
     * @var array<int>
     * @phpstan-ignore-next-line no le pongo un valor por defecto porque falla
     */
    public array $selectedKeys;

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name'),
            Column::make('Activo', 'active')->view('bpanel4-product-brands::bpanel.livewire.datatable-columns.active'),
            Column::make('Acciones', 'id')->view('bpanel4-product-brands::bpanel.livewire.datatable-columns.actions'),
        ];
    }

    /**
     * @return Builder<ProductBrand>
     */
    public function query(): Builder
    {
        return ProductBrand::query()
            ->orderBy('name', 'DESC')
            ->when(
                $this->getAppliedFilterWithValue('search'),
                fn (Builder $query, string|int|null $term): Builder => $query->where('name', 'like', '%' . $term . '%')
            );
    }

    /**
     * @return array{bulkDelete: string}
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            ProductBrand::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
