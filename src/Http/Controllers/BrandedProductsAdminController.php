<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Http\Controllers;

use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Bittacora\Bpanel4\Products\Http\Controllers\ProductAdminController;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\View\View;
use RuntimeException;

final class BrandedProductsAdminController extends ProductAdminController
{
    public function create(?View $view = null): View
    {
        if (null !== $this->decorated) {
            $view = $this->decorated->create($view);
        }

        if (!$view instanceof View) {
            throw new RuntimeException();
        }

        return $view->nest(
            'before-active-additional-fields',
            'bpanel4-product-brands::bpanel.product-fields.create',
            $view->getData() + [
                'brand' => null,
                'brands' => ProductBrand::where('active', true)->pluck('name', 'id'),
            ],
        );
    }

    public function edit(Product $model, string $locale = 'es', ?View $view = null): View
    {
        if (null !== $this->decorated) {
            $view = $this->decorated->edit($model, $locale, $view);
        }

        $this->checkDecoratorValidity($view);

        return $view->nest(
            'before-active-additional-fields',
            'bpanel4-product-brands::bpanel.product-fields.edit',
            $view->getData() + [
                'brand' => null,
                'brands' => ProductBrand::where('active', true)->pluck('name', 'id'),
            ],
        );
    }
}
