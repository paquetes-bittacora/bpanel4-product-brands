<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\ProductBrands\Actions\CreateProductBrand;
use Bittacora\Bpanel4\ProductBrands\Actions\UpdateProductBrand;
use Bittacora\Bpanel4\ProductBrands\Dtos\ProductBrandDto;
use Bittacora\Bpanel4\ProductBrands\Http\Requests\CreateBrandRequest;
use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Throwable;

final class ProductBrandsAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly DatabaseManager $db,
        private readonly ExceptionHandler $exceptionHandler,
        private readonly Redirector $redirector,
    ) {
    }

    /**
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('bpanel4-product-brands.index');
        return $this->view->make('bpanel4-product-brands::bpanel.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create(): View
    {
        $this->authorize('bpanel4-product-brands.create');
        return $this->view->make('bpanel4-product-brands::bpanel.create', [
            'action' => '',
            'brand' => null,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function store(CreateBrandRequest $request, CreateProductBrand $createProductBrand): RedirectResponse
    {
        $this->authorize('bpanel4-product-brands.store');
        $this->db->beginTransaction();
        try {
            $dto = ProductBrandDto::fromArray($request->validated());
            $createProductBrand->execute($dto);
            $this->db->commit();
            return $this->redirector->route('bpanel4-product-brands.index')
                ->with(['alert-success' => 'La marca se creó correctamente']);
        } catch (Throwable $exception) {
            $this->exceptionHandler->report($exception);
            $this->db->rollBack();
            return $this->redirector->back()->with(['alert-danger' => 'Ocurrió un error al crear la marca']);
        }
    }

    public function edit(ProductBrand $productBrand): View
    {
        return $this->view->make('bpanel4-product-brands::bpanel.edit', [
            'action' => '',
            'brand' => $productBrand,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function update(ProductBrand $productBrand, CreateBrandRequest $request, UpdateProductBrand $updateProductBrand): RedirectResponse
    {
        $this->authorize('bpanel4-product-brands.update');
        $this->db->beginTransaction();
        try {
            $data = $request->validated();
            $data['id'] = $productBrand->getId();
            $dto = ProductBrandDto::fromArray($data);
            $updateProductBrand->execute($dto);
            $this->db->commit();
            return $this->redirector->route('bpanel4-product-brands.index')
                ->with(['alert-success' => 'La marca se editó correctamente']);
        } catch (Throwable $exception) {
            $this->exceptionHandler->report($exception);
            $this->db->rollBack();
            return $this->redirector->back()->with(['alert-danger' => 'Ocurrió un error al editar la marca']);
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(ProductBrand $productBrand): RedirectResponse
    {
        $this->authorize('bpanel4-product-brands.destroy');
        $productBrand->delete();

        return $this->redirector->back()->with(['alert-success' => 'Marca eliminada']);
    }
}
