<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @method string[] validated($key = null, $default = null)
 */
final class CreateBrandRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array{name: string, active: string}
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'active' => 'required|boolean',
        ];
    }
}
