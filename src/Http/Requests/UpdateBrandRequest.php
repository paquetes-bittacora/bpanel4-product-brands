<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @method string[] validated($key = null, $default = null)
 */
final class UpdateBrandRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array{name: string, active: string}
     */
    public function rules(): array
    {
        return [
            'id' => 'required|exists:brands,id',
            'name' => 'required|string',
            'active' => 'required|boolean',
        ];
    }
}
