<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Traits;

use Bittacora\Bpanel4\ProductBrands\Models\ProductBrand;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait HasBrand
{
    public function brand(): BelongsToMany
    {
        return $this->belongsToMany(ProductBrand::class);
    }
}
