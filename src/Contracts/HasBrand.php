<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Contracts;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface HasBrand
{
    public function brand(): BelongsToMany;
}
