<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands;

use Bittacora\Bpanel4\ProductBrands\Commands\InstallCommand;
use Bittacora\Bpanel4\ProductBrands\Contracts\HasBrand;
use Bittacora\Bpanel4\ProductBrands\Http\Controllers\BrandedProductsAdminController;
use Bittacora\Bpanel4\ProductBrands\Http\Livewire\BrandsDatatable;
use Bittacora\Bpanel4\ProductBrands\Listeners\SaveProductBrand;
use Bittacora\Bpanel4\ProductBrands\Services\BrandsProductFilter;
use Bittacora\Bpanel4\Products\Events\ProductCreated;
use Bittacora\Bpanel4\Products\Events\ProductUpdated;
use Bittacora\Bpanel4\Products\Http\Controllers\ProductAdminController;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Livewire\LivewireManager;

final class Bpanel4ProductBrandsServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-product-brands';

    public function register(): void
    {
        parent::register();

        $this->app->extend(
            ProductAdminController::class,
            static function (ProductAdminController $controller, Application $app) {
                $newController = $app->make(BrandedProductsAdminController::class);
                $newController->setDecorated($controller);
                return $newController;
            }
        );

        $this->app->tag(BrandsProductFilter::class, 'products-filter');
    }

    public function boot(
        LivewireManager $livewire,
        Dispatcher $eventDispatcher,
    ): void {
        $this->commands(InstallCommand::class);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->app->bind(HasBrand::class, Product::class);
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);

        $livewire->component(self::PACKAGE_PREFIX . '::livewire.brands-table', BrandsDatatable::class);
        $eventDispatcher->listen(ProductCreated::class, SaveProductBrand::class);
        $eventDispatcher->listen(ProductUpdated::class, SaveProductBrand::class);
    }
}
