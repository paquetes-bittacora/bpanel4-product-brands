<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\Translatable\HasTranslations;

/**
 * Bittacora\Bpanel4\ProductBrands\Models\ProductBrand
 *
 * @property int $id
 * @property string $name
 * @property bool|string $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ProductBrand newModelQuery()
 * @method static Builder|ProductBrand newQuery()
 * @method static Builder|ProductBrand query()
 * @method static Builder|ProductBrand whereCreatedAt($value)
 * @method static Builder|ProductBrand whereId($value)
 * @method static Builder|ProductBrand whereName($value)
 * @method static Builder|ProductBrand whereUpdatedAt($value)
 * @method static where(string $string, true $true)
 * @mixin \Eloquent
 */
final class ProductBrand extends Model
{
    use HasTranslations;

    /** @var string[]  */
    public array $translatable = ['name'];

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function isActive(): bool
    {
        return (bool) $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }
}
