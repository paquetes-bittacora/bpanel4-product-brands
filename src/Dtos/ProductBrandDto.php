<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Dtos;

use Bittacora\Dtos\Dto;

final class ProductBrandDto extends Dto
{
    public function __construct(
        public readonly string $name,
        public readonly bool $active,
        public readonly ?int $id = null,
    ) {
    }
}
