<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductBrands\Listeners;

use Bittacora\Bpanel4\Products\Events\ProductCreated;
use Bittacora\Bpanel4\Products\Events\ProductUpdated;

final class SaveProductBrand
{
    public function handle(ProductCreated|ProductUpdated $event): void
    {
        /** @var string|null $requestBrand */
        $requestBrand = $event->getRequest()->get('brand');
        $product = $event->getProduct();

        if (null === $requestBrand || "" === $requestBrand) {
            $product->brand()->sync([]);
            return;
        }

        $product->brand()->sync([(int) $requestBrand]);
    }
}