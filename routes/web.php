<?php

declare(strict_types=1);

use Bittacora\Bpanel4\ProductBrands\Http\Controllers\ProductBrandsAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('/marcas')->name('bpanel4-product-brands.')->middleware(['web', 'auth', 'admin-menu'])->group(function (): void {
    Route::get('/listar', [ProductBrandsAdminController::class, 'index'])->name('index');
    Route::get('/crear', [ProductBrandsAdminController::class, 'create'])->name('create');
    Route::post('/crear', [ProductBrandsAdminController::class, 'store'])->name('store');
    Route::get('/{productBrand}/editar', [ProductBrandsAdminController::class, 'edit'])->name('edit');
    Route::post('/{productBrand}/editar', [ProductBrandsAdminController::class, 'update'])->name('update');
    Route::delete('/{productBrand}/eliminar', [ProductBrandsAdminController::class, 'destroy'])->name('destroy');
});
